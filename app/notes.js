'use strict';
(function () {
  angular.module('notes',['requests','CRUD'])
  .controller('categoriesController',['$rootScope','request','CRUD',function(scope,request,crud){
    var host = 'http://localhost:19170/';
    scope.category = request(
      host,
      {plural:"categories",singular:"category"},
      'categoriesCollection');
    scope.category.readAll();
    this.crud=crud('categoriesCollection',
    {
      categoria: "",
      color: "#ffffff",
      descripcion: ""
    },
    scope.category);
    this.crud.to('c');
  }])
  .controller('notesController',['$rootScope','request','CRUD',function(scope,request,crud){
    var host = 'http://localhost:19170/';
    scope.note = request(
      host,
      {plural:"notes",singular:"note"},
      'notesCollection');
    scope.note.category = request(
      host,
      {plural:"categories",singular:"category"},
      'categoryOfNote');
    scope.note.category.getBy=function(element){
      angular.forEach(scope.categoriesCollection.data.body,function(category){
        if(category._id===element.category_id){
          element.category={
            color:category.color,
            categoria:category.categoria
          };
        }
      });
    }
    scope.note.readAll();
    this.crud=crud('notesCollection',
    {
      category_id: "",
      titulo: "",
      cuerpo: ""
    },
    scope.note);
    this.crud.to('c');
  }])
})();
