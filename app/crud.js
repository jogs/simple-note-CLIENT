'use strict';
(function() {
  angular.module('CRUD',[])
  .factory('CRUD',['$rootScope',function(scope){
    return function(objectContainer,objectBase,requestObject){
      scope[objectContainer].current={
        body:{},
        action:{},
        status:0
      }
      if(requestObject){
        requestObject.present=true
      }
      requestObject=requestObject||{present:false};
      objectBase=objectBase||{}
      return {
        to:function(action,object){
          scope[objectContainer].current.body=object||objectBase;
          if(action.toLowerCase()==='c'||action.toLowerCase()==='r'||action.toLowerCase()==='u'||action.toLowerCase()==='d'){
            switch (action.toUpperCase()) {
              case 'C':
                scope[objectContainer].current.action={name:'CREATE',type:0};
                break;
              case 'R':
                scope[objectContainer].current.action={name:'READ',type:1};
                break;
              case 'U':
                scope[objectContainer].current.action={name:'UPDATE',type:2};
                break;
              default:
                scope[objectContainer].current.action={name:'DELETE',type:3};
            }
            return true;
          }
          else{
            return false;
          }
        },
        run:function(){
          if(requestObject.present){
            switch (scope[objectContainer].current.action.type) {
              case 0:
                requestObject.createOne(scope[objectContainer].current.body);
                break;
              case 1:
                requestObject.readOne(scope[objectContainer].current.body._id);
                break;
              case 2:
                requestObject.updateOne(scope[objectContainer].current.body._id,scope[objectContainer].current.payload);
                break;
              default:
              requestObject.deleteOne(scope[objectContainer].current.body._id);
            }
            return true;
          }
          else{
            return false;
          }
        }
      }

    }
  }])
})();
