'use strict';
(function () {
  angular.module('requests',[])
  // .config(['$httpProvider', function($httpProvider) {
  //     $httpProvider.defaults.useXDomain = true;
  //     delete $httpProvider.defaults.headers.common['X-Requested-With'];
  //   }
  // ])
  .factory('request',['$rootScope','$http',function(scope,http){
    return function(host,resource,container){
      scope[container]={
        data:{},
        current:{}
      }
      return {
        readAll:function(){
          http.get(host+resource.plural)
          .then(function(response){
            scope[container].data.status = response.status;
            if(response.status===200){
              scope[container].data.body = response.data.body
            }
            else if(response.status===204){
              scope[container].data.body = []
            }
            console.log(scope[container].data);
          })
        },
        readOne:function(id){
          http.get(host+resource.singular+'/'+id)
          .then(function(response){
            scope[container].current.status = response.status;
            if(response.status===200){
              scope[container].current.body = response.data.body
            }
            console.log(scope[container].current);
          })
        },
        createOne:function(entity){
          console.log(entity);
          http.post(host+resource.singular,entity)
          .then(function(response){
            scope[container].current.status = response.status;
            if(response.status===201){
              scope[container].current.body = response.data.body
            }
            console.log(scope[container].current);
          })
        },
        updateOne:function(id,payload){
          http.put(host+resource.singular+'/'+id,payload)
          .then(function(response){
            scope[container].current.status = response.status;
            if(response.status===200){
              scope[container].current.body = response.data.body
            }
            console.log(scope[container].current);
          })
        },
        deleteOne:function(id){
          http.delete(host+resource.singular+'/'+id)
          .then(function(response){
            scope[container].current.status = response.status;
            if(response.status===200){
              scope[container].current.body = response.data.body
            }
            console.log(scope[container].current);
          })
        }
      }
    }
  }])
})();
